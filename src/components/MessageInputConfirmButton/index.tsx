import React from 'react';

import './index.css';

type MessageInputConfirmButtonProps = {
  title: string,
  callback(): void,
  isBlocked: boolean
}

const MessageInputConfirmButton: React.FunctionComponent<MessageInputConfirmButtonProps> = (
  {
    title,
    callback,
    isBlocked
  }
) => {
  let className: string = 'message-confirm-button message-input-button';
  if (isBlocked) {
    className += ' message-input-blocked';
  }

  return (
    <button
      type="button"
      className={className}
      onClick={callback}
    >
      {title}
    </button>
  );
};

export default MessageInputConfirmButton;
