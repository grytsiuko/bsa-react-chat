import React from 'react';

import './index.css';
import {IMessage} from '../../service/messageService';
import {parseDateTime} from '../../utils/datetime';
import MessageContentIcon from '../MessageContentIcon';

type MessageContentProps = {
  message: IMessage
  currentUserMessage: boolean
  isEditing: boolean
  isLiked: boolean
  deleteMessage(): void,
  editMessage(): void,
  toggleLike(): void
}

const MessageContent: React.FunctionComponent<MessageContentProps> = (
  {
    message,
    currentUserMessage,
    isEditing,
    isLiked,
    deleteMessage,
    editMessage,
    toggleLike
  }
) => {
  const time: string = (message.editedAt === '')
    ? parseDateTime(message.createdAt)
    : `Edited at ${parseDateTime(message.editedAt)}`;

  return (
    <div className="message-content">
      {!currentUserMessage && <div className="message-user">{message.user}</div>}
      <div className="message-text">{message.text}</div>
      <div className="message-bottom">
        {currentUserMessage && <MessageContentIcon icon="fas fa-trash" active={false} callback={deleteMessage} />}
        {currentUserMessage && <MessageContentIcon icon="fas fa-edit" active={isEditing} callback={editMessage} />}
        <div className="message-time">{time}</div>
        {!currentUserMessage && <MessageContentIcon icon="fas fa-thumbs-up" active={isLiked} callback={toggleLike} />}
      </div>
    </div>
  );
};

export default MessageContent;
