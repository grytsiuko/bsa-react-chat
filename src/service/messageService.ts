import {v1 as uuid} from 'uuid';

import callApi from './apiHelper';

// mocks:
const currentUserId: string = uuid();
const currentUserAvatar: string = uuid();
const currentUserName: string = 'BSA';

export interface IMessage {
  id: string,
  userId: string,
  avatar: string,
  user: string,
  text: string,
  createdAt: string,
  editedAt: string,
  newDay?: boolean
}

export type IMessageArray = Array<IMessage>

export const isCurrentUserMessage = (message: IMessage):boolean => {
  return message.userId === currentUserId;
};

export const getMessages = async (): Promise<IMessageArray> => {
  const response = await callApi({
    endpoint: 'https://edikdolynskyi.github.io/react_sources/messages.json',
    method: 'GET'
  });
  return response.json();
};

export const addMessage = (text: string): IMessage => {
  console.log(`Here will be Add message ${text} backend request`);
  // mock:
  return {
    id: uuid(),
    text: text,
    user: currentUserName,
    avatar: currentUserAvatar,
    userId: currentUserId,
    editedAt: '',
    createdAt: new Date().toUTCString()
  };
};

export const updateMessage = (message: IMessage, text: string): IMessage => {
  console.log(`Here will be Update message ${message.id} backend request`);
  // mock:
  return {
    ...message,
    text,
    editedAt: new Date().toUTCString()
  };
};

export const deleteMessage = (id: string): void => {
  console.log(`Here will be Delete message ${id} backend request`);
};

export const toggleLikeMessage = (id: string): void => {
  console.log(`Here will be Toggle like ${id} backend request`);
};
