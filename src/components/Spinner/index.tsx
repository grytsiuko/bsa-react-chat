import React from 'react';

import './index.css';

const Spinner: React.FunctionComponent = () => (
  <div className="spinner-wrapper">
    <img alt="Loading..." src="spinner.gif" className="spinner" />
  </div>
);

export default Spinner;
