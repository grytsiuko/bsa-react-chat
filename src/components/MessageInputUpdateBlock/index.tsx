import React from 'react';
import MessageInputConfirmButton from '../MessageInputConfirmButton';
import MessageInputSimpleButton from '../MessageInputSimpleButton';

type MessageInputUpdateBlockProps = {
  updateMessage(): void,
  cancelEdit(): void,
  messageText: string
}

const MessageInputUpdateBlock: React.FunctionComponent<MessageInputUpdateBlockProps> = (
  {
    updateMessage,
    cancelEdit,
    messageText
  }
) => (
  <div className="message-input-button-wrapper">
    <MessageInputConfirmButton
      callback={updateMessage}
      isBlocked={messageText.trim() === ''}
      title="Update"
    />
    <MessageInputSimpleButton
      title="Cancel"
      callback={cancelEdit}
    />
  </div>
);

export default MessageInputUpdateBlock;
