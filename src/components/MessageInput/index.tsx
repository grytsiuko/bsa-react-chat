import React, { useState } from 'react';

import './index.css';
import MessageInputSendBlock from '../MessageInputSendBlock';
import MessageInputUpdateBlock from '../MessageInputUpdateBlock';

export interface IEditingMessage {
  id: string,
  text: string
}

type MessageInputProps = {
  addMessage(text: string): void,
  updateMessage(message: IEditingMessage): void,
  cancelEditMessage(): void,
  messageToEdit: IEditingMessage | null
}

const MessageInput: React.FunctionComponent<MessageInputProps> = (
  {
    addMessage,
    updateMessage,
    cancelEditMessage,
    messageToEdit
  }
) => {
  const [newMessageText, setNewMessageText] = useState<string>('');
  const [editingMessage, setEditingMessage] = useState<IEditingMessage | null>(null);

  if (messageToEdit && (!editingMessage || messageToEdit.id !== editingMessage.id)) {
    setEditingMessage(messageToEdit);
  }

  if (!messageToEdit && editingMessage) {
    setEditingMessage(null);
  }

  const handleChange = (value: string): void => {
    if (editingMessage) {
      setEditingMessage({ ...editingMessage, text: value });
    } else {
      setNewMessageText(value);
    }
  };

  const handleSend = (): void => {
    if (newMessageText.trim()) {
      addMessage(newMessageText);
      setNewMessageText('');
    }
  };

  const handleUpdate = (): void => {
    if (editingMessage?.text.trim()) {
      updateMessage(editingMessage);
      setEditingMessage(null);
    }
  };

  const handleCancelEdit = (): void => {
    cancelEditMessage();
  };

  const handleTextAreaKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>): void => {
    if (e.keyCode === 13 && !e.shiftKey) {
      e.preventDefault();
      if (editingMessage) {
        handleUpdate();
      } else {
        handleSend();
      }
    }
  };

  return (
    <div className="message-input">
      <textarea
        className="message-input-area"
        placeholder="Type your message here. Press Enter to confirm, Shift+Enter to switch to a new line."
        onChange={e => handleChange(e.target.value)}
        onKeyDown={e => handleTextAreaKeyPress(e)}
        value={editingMessage ? editingMessage.text : newMessageText}
      />
      {
        editingMessage
          ? (
            <MessageInputUpdateBlock
              updateMessage={() => handleUpdate()}
              cancelEdit={() => handleCancelEdit()}
              messageText={editingMessage.text}
            />
          )
          : (
            <MessageInputSendBlock
              sendMessage={() => handleSend()}
              messageText={newMessageText}
            />
          )
      }
    </div>
  );
};

export default MessageInput;
