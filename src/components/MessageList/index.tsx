import React, { useRef, useEffect } from 'react';

import './index.css';
import Message from '../Message';
import { IMessage, IMessageArray } from '../../service/messageService';
import MessageListHr from '../MessageListHr';
import { parseDate, sameDay } from '../../utils/datetime';
import { IEditingMessage } from '../MessageInput';

type MessageListProps = {
  messages: IMessageArray | null,
  editingId: string | null,
  editMessage(editingMessage: IEditingMessage): void,
  deleteMessage(id: string): void,
  scrollOnRender: boolean
}

const MessageList: React.FunctionComponent<MessageListProps> = (
  {
    messages,
    editingId,
    editMessage,
    deleteMessage,
    scrollOnRender
  }
) => {
  const messagesEndRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = () => {
    if (scrollOnRender && messagesEndRef?.current) {
      messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(scrollToBottom);

  const buildMessageListHr = (message: IMessage): JSX.Element => (
    <MessageListHr
      key={message.createdAt}
      date={parseDate(message.createdAt)}
    />
  );

  const buildMessage = (message: IMessage): JSX.Element => (
    <Message
      key={message.id}
      isEditing={editingId === message.id}
      message={message}
      deleteMessage={deleteMessage}
      editMessage={editMessage}
    />
  );

  const buildMessageList = (): Array<JSX.Element> => {
    const list: Array<JSX.Element> = [];
    if (messages) {
      for (let i: number = 0; i < messages.length; i++) {
        const currMessage: IMessage = messages[i];
        if (i === 0 || !sameDay(messages[i - 1], currMessage)) {
          list.push(buildMessageListHr(currMessage));
        }
        list.push(buildMessage(currMessage));
      }
    }
    return list;
  };

  return (
    <div className="message-list-wrapper">
      <div className="message-list">
        {buildMessageList()}
        <div ref={messagesEndRef} />
      </div>
    </div>
  );
};

export default MessageList;
