import React from 'react';

import './index.css';

type MessageContentIconProps = {
  icon: string,
  active: boolean,
  callback(): void
}

const MessageContentIcon: React.FunctionComponent<MessageContentIconProps> = (
  {
    icon,
    active,
    callback
  }
) => {
  let className: string = `message-icon ${icon}`;
  if (active) {
    className += ' message-icon-active';
  }

  return (
    <span
      className={className}
      onClick={callback}
    />
  );
};

export default MessageContentIcon;
