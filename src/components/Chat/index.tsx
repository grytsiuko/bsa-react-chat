import React, {useState} from 'react';
import Header from '../Header';
import MessageList from '../MessageList';
import MessageInput, {IEditingMessage} from '../MessageInput';

import './index.css';
import Spinner from '../Spinner';
import { addMessage, updateMessage, deleteMessage, getMessages, IMessageArray } from '../../service/messageService';

const Chat: React.FunctionComponent = () => {
  const [messages, setMessages] = useState<IMessageArray | null>(null);
  const [scrollOnRender, setScrollOnRender] = useState<boolean>(false);
  const [editingMessage, setEditingMessage] = useState<IEditingMessage | null>(null);

  if (!messages) {
    getMessages()
      .then((messageArray: IMessageArray) => {
        messageArray.sort((a, b) => (a.createdAt > b.createdAt ? 1 : 0));
        setMessages(messageArray);
        setScrollOnRender(true);
      });
  }

  const handleAddMessage = (text: string): void => {
    if (messages !== null) {
      setMessages([...messages, addMessage(text)]);
      setScrollOnRender(true);
    }
  };

  const handleUpdateMessage = (editedMessage: IEditingMessage): void => {
    if (messages !== null) {
      setMessages(messages.map(m => (m.id !== editedMessage.id ? m : updateMessage(m, editedMessage.text))));
      setScrollOnRender(false);
      setEditingMessage(null);
    }
  };

  const handleEditMessage = (editedMessage: IEditingMessage): void => {
    setEditingMessage(editedMessage);
    setScrollOnRender(false);
  };

  const handleCancelEditMessage = (): void => {
    setEditingMessage(null);
    setScrollOnRender(false);
  };

  const handleDeleteMessage = (id: string): void => {
    if (messages !== null) {
      deleteMessage(id);
      setMessages([...messages].filter(m => m.id !== id));
      setScrollOnRender(false);
      if (editingMessage && editingMessage.id === id) {
        setEditingMessage(null);
      }
    }
  };

  return (
    <div className="chat-wrapper">
      <div className="chat">
        <Header messages={messages} />
        <MessageList
          messages={messages}
          editingId={editingMessage ? editingMessage.id : null}
          deleteMessage={id => handleDeleteMessage(id)}
          editMessage={editedMessage => handleEditMessage(editedMessage)}
          scrollOnRender={scrollOnRender}
        />
        <MessageInput
          addMessage={text => handleAddMessage(text)}
          cancelEditMessage={() => handleCancelEditMessage()}
          updateMessage={editedMessage => handleUpdateMessage(editedMessage)}
          messageToEdit={editingMessage}
        />
        {!messages && <Spinner />}
      </div>
    </div>
  );
};

export default Chat;
