import React from 'react';

import './index.css';

type MessageListHrProps = {
  date: string,
}

const MessageListHr: React.FunctionComponent<MessageListHrProps> = (
  {
    date
  }
) => (
  <div className="message-hr-wrapper">
    <hr className="message-hr"/>
    <div className="message-hr-title-wrapper">
      <div className="message-hr-title">
        {date}
      </div>
    </div>
  </div>
);

export default MessageListHr;
