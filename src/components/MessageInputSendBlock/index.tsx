import React from 'react';
import MessageInputConfirmButton from '../MessageInputConfirmButton';

type MessageInputSendBlockProps = {
  sendMessage(): void,
  messageText: string
}

const MessageInputSendBlock: React.FunctionComponent<MessageInputSendBlockProps> = (
  {
    sendMessage,
    messageText
  }
) => (
  <div className="message-input-button-wrapper">
    <MessageInputConfirmButton
      callback={sendMessage}
      isBlocked={messageText.trim() === ''}
      title="Send"
    />
  </div>
);

export default MessageInputSendBlock;
