import React from 'react';

import './index.css';

type MessageInputSimpleButtonProps = {
  title: string,
  callback(): void
}

const MessageInputSimpleButton: React.FunctionComponent<MessageInputSimpleButtonProps> = (
  {
    title,
    callback
  }
) => (
  <button
    type="button"
    className="message-simple-button message-input-button"
    onClick={callback}
  >
    {title}
  </button>
);

export default MessageInputSimpleButton;
