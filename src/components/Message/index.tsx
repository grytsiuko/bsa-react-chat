import React, { useState } from 'react';

import './index.css';
import { isCurrentUserMessage, IMessage, toggleLikeMessage } from '../../service/messageService';
import { IEditingMessage } from '../MessageInput';
import MessageAvatar from '../MessageAvatar';
import MessageContent from '../MessageContent';

type MessageProps = {
  message: IMessage,
  isEditing: boolean,
  editMessage(editingMessage: IEditingMessage): void,
  deleteMessage(id: string): void
}

const Message: React.FunctionComponent<MessageProps> = (
  {
    message,
    isEditing,
    editMessage,
    deleteMessage
  }
) => {
  const [liked, setLiked] = useState<boolean>(false);

  const currentUserMessage: boolean = isCurrentUserMessage(message);

  const toggleLike = (): void => {
    toggleLikeMessage(message.id);
    setLiked(!liked);
  };

  let wrapperClassName: string = 'message-wrapper';
  if (currentUserMessage) {
    wrapperClassName += ' message-wrapper-right';
  }

  return (
    <div>
      <div className={wrapperClassName}>
        <div className="message">
          {!currentUserMessage && <MessageAvatar message={message} />}
          <MessageContent
            message={message}
            currentUserMessage={currentUserMessage}
            isEditing={isEditing}
            isLiked={liked}
            deleteMessage={() => deleteMessage(message.id)}
            editMessage={() => editMessage({ id: message.id, text: message.text })}
            toggleLike={() => toggleLike()}
          />
        </div>
      </div>
    </div>
  );
};

export default Message;
